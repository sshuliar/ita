provider "aws" {
  region = "${var.region}"
}

module "network" {
  source = "010_network"
  environment_id = "${var.environment_id}"
  environment_name = "${var.environment_name}"
  vpc_cidr = "${var.vpc_cidr}"
  public_key_path = "~/.ssh/id_rsa.pub"
  ita_group = "${var.ita_group}"
}

module "route53" {
  source = "011-route-53"
  environment_id = "${var.environment_id}"
  environment_name = "${var.environment_name}"
  main_zone = "ssh.if.ua"
  sub_zone_name = "ita"
  ita_group = "${var.ita_group}"
}

module "rds" {
  source = "020-rds"
  environment_id = "${var.environment_id}"
  environment_name = "${var.environment_name}"
  vpc_cidr = "${module.network.vpc_cidr}"
  private_subnets_ids = "${module.network.private_subnet_ids}"
  ita_group = "${var.ita_group}"
  db_private_security_groups_ids = "${module.network.private_security_group_ids}"
}