resource "aws_internet_gateway" "default_igw" {
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "${var.environment_name} VPC"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

// elastic IPs
resource "aws_eip" "nat" {
  count = "${length(var.zones) * var.enable_nat_gateway}"
  vpc = true
  tags {
    Name = "${var.environment_name} VPC"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

// attach managed NAT
resource "aws_nat_gateway" "nat" {
  count = "${length(var.zones) * var.enable_nat_gateway}"
  allocation_id = "${element(aws_eip.nat.*.id, count.index)}"
  subnet_id = "${element(aws_subnet.public.*.id, count.index)}"
  tags {
    Name = "${var.environment_name} NAT"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}