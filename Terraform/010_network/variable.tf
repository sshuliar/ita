variable "environment_name" {}
variable "environment_id" {}

variable "ita_group" {}

variable "vpc_cidr" {}
variable "zones" {
  type = "list"
  default = [
    "us-east-1b",
    "us-east-1c",
    "us-east-1d",
    "us-east-1e"
  ]
  description = "List of availability zones"
}

variable "public_key_path" {
  description = "Enter the path to the SSH Public Key to add to AWS."
  default = "~/.ssh/id_rsa.pub"
}

variable "nat_ssh_cidrs" {
  type = "list"
  default = []
}

variable "open_selective_ports" { default = false }
variable "on_premise_cidr" { default = ""}

variable "enable_nat_gateway" { default = 0 }