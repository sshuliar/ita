// Security groups

// Private that allows access from the VPC
resource "aws_security_group" "private" {
  vpc_id = "${aws_vpc.main.id}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["${aws_vpc.main.cidr_block}"]
  }
  tags {
    Name = "${var.environment_name} Private Security Group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

// Privat that allow accress from on-premice network
resource "aws_security_group" "private_on_premise_access" {
  count = "${var.on_premise_cidr == "" ? 0 : 1}"
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "${var.environment_name} Private on-premise-access Security Group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

resource "aws_security_group_rule" "ingress_all" {
  count = 0 // "${1 * (var.open_selective_ports == false ? 0 : 1) * (var.on_premise_cidr == "" ? 0 : 1)} "
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["${var.on_premise_cidr}"]
  security_group_id = "${aws_security_group.private_on_premise_access.id}"
  type = "ingress"
}

// SSH
resource "aws_security_group_rule" "ingress_ssh" {
  count = "${var.open_selective_ports  * (var.on_premise_cidr == "" ? 0 : 1)}"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["${var.on_premise_cidr}"]
  security_group_id = "${aws_security_group.private_on_premise_access.id}"
  type = "ingress"
}

//  HTTPS
resource "aws_security_group_rule" "ingress_ssl" {
  count = "${var.open_selective_ports  * (var.on_premise_cidr == "" ? 0 : 1)}"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  cidr_blocks = ["${var.on_premise_cidr}"]
  security_group_id = "${aws_security_group.private_on_premise_access.id}"
  type = "ingress"
}

resource "aws_security_group" "public" {
  vpc_id = "${aws_vpc.main.id}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_groups = ["${aws_security_group.private.id}"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = ["${aws_security_group.public_http.id}"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    security_groups = ["${aws_security_group.public_http.id}"]
  }
  tags {
    Name = "${var.environment_name} Public Security Group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

resource "aws_security_group" "nat_ssh" {
  vpc_id = "${aws_vpc.main.id}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${aws_vpc.main.cidr_block}"] //"${var.nat_ssh_cidrs}",
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["${aws_vpc.main.cidr_block}"] //"${var.nat_ssh_cidrs}",
  }
  tags {
    Name = "${var.environment_name} NAT SSH Security Group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

resource "aws_security_group" "public_http" {
  vpc_id = "${aws_vpc.main.id}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.environment_name} Public HTTP(S) Security Group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}