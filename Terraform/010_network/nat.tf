// Bastion instances - used only for SSH tunneling for Terraform provisioning

data "aws_ami" "nat_ami" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn-ami-vpc-nat*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  "owners" = ["amazon"]
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "${var.environment_id}-ssh-key"
  public_key = "${file(var.public_key_path)}"
}

resource "aws_instance" "nat" {
  instance_type = "t2.micro"
  count = 1
  availability_zone = "${element(var.zones, count.index)}"
  ami = "${data.aws_ami.nat_ami.id}"
  source_dest_check = false
  subnet_id = "${element(aws_subnet.public.*.id, count.index)}"
  vpc_security_group_ids = ["${aws_security_group.public.id}", "${aws_security_group.nat_ssh.id}"]
  key_name = "ssh"
  associate_public_ip_address = true
  lifecycle {
    ignore_changes = ["ami"]
  }
  tags {
    Name = "${var.environment_name} NAT Instance"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
  volume_tags {
    Name = "${var.environment_name} NAT VOLUME"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

