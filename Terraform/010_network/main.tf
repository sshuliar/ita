resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr}"
  tags {
    Name = "${var.environment_name} VPC"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}
