// public
resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.main.id}"
  count = "${length(var.zones)}"
  cidr_block = "${cidrsubnet(var.vpc_cidr, 4, count.index * 2)}"
  availability_zone = "${element(var.zones, count.index)}"
  tags {
    Name = "${var.environment_name} Public subnet"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

// private
resource "aws_subnet" "private" {
  vpc_id = "${aws_vpc.main.id}"
  count = "${length(var.zones)}"
  cidr_block = "${cidrsubnet(var.vpc_cidr, 4, (count.index * 2 + 1))}"
  availability_zone = "${element(var.zones, count.index)}"

  tags {
    Name = "${var.environment_name} Private subnet"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}
