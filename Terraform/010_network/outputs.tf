output "vpc_id" {
  value = "${aws_vpc.main.id}"
}
output "vpc_cidr" {
  value = "${var.vpc_cidr}"
}
output "public_subnet_ids" {
  value = ["${aws_subnet.public.*.id}"]
}
output "private_subnet_ids" {
  value = ["${aws_subnet.private.*.id}"]
}
output "public_security_group_ids" {
  value = ["${aws_security_group.public.*.id}"]
}
output "private_security_group_ids" {
  value = ["${concat(aws_security_group.private.*.id, aws_security_group.private_on_premise_access.*.id)}"]
}
output "ssh_security_group_ids" {
  value = ["${aws_security_group.nat_ssh.*.id}"]
}
output "https_security_group_id" {
  value = "${aws_security_group.public_http.id}"
}
output "nat_elastic_ips" {
  value = ["${aws_instance.nat.*.public_ip}"]
}
output "nat_private_ips" {
  value = ["${aws_instance.nat.*.private_ip}"]
}
output "public_subnet_route_ids" {
  value = ["${aws_route_table.public.*.id}"]
}
output "private_subnet_route_ids" {
  value = ["${aws_route_table.private.*.id}"]
}
output "environment_id" {
  value = "${var.environment_id}"
}
output "environment_name" {
  value = "${var.environment_name}"
}
output "zones" {
  value = "${var.zones}"
}
output "nat_key" {
  value = "${aws_key_pair.ssh_key.id}"
}