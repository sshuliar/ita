variable "region" {}

variable "environment_name" {}
variable "environment_id" {}

variable "ita_group" {}

variable "read_capacity" {
  default = 1
}
variable "write_capacity" {
  default = 1
}