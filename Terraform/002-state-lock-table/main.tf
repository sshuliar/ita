provider "aws" {
  region = "${var.region}"
}

resource "aws_dynamodb_table" "terraform_statelock" {
  name           = "${var.environment_id}-tf-lock"
  read_capacity  = "${var.read_capacity}"
  write_capacity = "${var.write_capacity}"
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "${var.environment_name} TF lock table"
    ita_group = "${var.ita_group}"
  }
}


