# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CREATE RDS Instance
# This template creates and provisions RDS instance
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

resource "aws_db_subnet_group" "subnet_group" {
  name       = "${var.environment_id}-rds-subnet-gr"
  subnet_ids = ["${var.private_subnets_ids}"]

  lifecycle {
    ignore_changes = ["description", "name"]
  }

  tags {
    Name = "${var.environment_name} subnet group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

resource "aws_db_parameter_group" "parameter_gr" {
  name   = "${var.environment_id}"
  family = "${var.db_parameter_group_family}"

  parameter {
    name  = "max_connections"
    value = "${var.db_max_connections}"
    apply_method = "pending-reboot"
  }

  parameter {
    name  = "rds.logical_replication"
    value = "1"
    apply_method = "pending-reboot"
  }

  parameter {
    name  = "track_activity_query_size"
    value = "16384"
    apply_method = "pending-reboot"
  }

  lifecycle {
    ignore_changes = ["description", "name"]
  }

  tags {
    Name = "${var.environment_name} parameter group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}

resource "aws_db_instance" "rds_instance" {
  allocated_storage           = "${var.db_storage}"
  engine                      = "${var.db_engine}"
  engine_version              = "${var.db_engine_version}"
  instance_class              = "${var.db_instance_type}"
  identifier                  = "${var.environment_id}"
  multi_az                    = "${var.db_multi_az}"
  publicly_accessible         = "${var.db_publicly_accessible}"
  skip_final_snapshot         = "${var.db_skip_final_snapshot}"
  db_subnet_group_name        = "${aws_db_subnet_group.subnet_group.id}"
  parameter_group_name        = "${aws_db_parameter_group.parameter_gr.name}"
  vpc_security_group_ids      = ["${var.db_private_security_groups_ids}"]
  storage_encrypted           = "${var.db_encryption}"
  iops                        = "${var.db_iops}"
  storage_type                = "${var.db_storage_type}"
  backup_retention_period     = "${var.db_snapshot_retention}"
  username                    = "postgres"
  password                    = "postgres"

  allow_major_version_upgrade = false
  auto_minor_version_upgrade  = false
  copy_tags_to_snapshot       = true

  lifecycle {
    ignore_changes = ["description", "name"]
  }

  tags {
    Name = "${var.environment_name} RDS Instance"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}