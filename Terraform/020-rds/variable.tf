variable "environment_name" {}
variable "environment_id" {}
variable "vpc_cidr" {}

variable "ita_group" {}

variable "db_instance_type" { default = "db.t2.micro" }
variable "db_engine" { default = "postgres" }
variable "db_engine_version" { default = "9.6" }
variable "db_parameter_group_family" { default = "postgres9.6" }
variable "db_storage" { default = 8 }
variable "db_snapshot" { default = "" }
variable "db_multi_az" { default = false}
variable "db_publicly_accessible" { default = false }
variable "db_skip_final_snapshot" { default = true }
variable "db_encryption" { default = false }
variable "db_storage_type" { default = "gp2" }
variable "db_iops" { default = 0 }
variable "db_snapshot_retention" { default = 1 }
variable "db_max_connections" { default = 500 }
variable "db_private_security_groups_ids" { type = "list" }
variable "private_subnets_ids" { type = "list" }
