db_instance_type = "db.t2.small"
db_engine = "postgres"
db_engine_version = "9.6.6"
db_parameter_group_family = "postgres9.6"
db_storage = "5"
db_max_connections = "1000"
db_multi_az = false
