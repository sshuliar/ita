variable "region" { default = "us-east-1"}

variable "environment_name" {}
variable "environment_id" {}

variable "ita_group" {}

variable "vpc_cidr" {}