variable "environment_name" {}
variable "environment_id" {}

variable "ita_group" {}

variable "main_zone" { default = ".com" }
variable "sub_zone_name" { default = "" }
