resource "aws_route53_zone" "zone" {
  name = "${var.sub_zone_name == "" ? var.environment_id : var.sub_zone_name}.${var.main_zone}"

  tags {
    Name = "${var.environment_name} subnet group"
    Environment = "${var.environment_id}"
    ita_group = "${var.ita_group}"
  }
}