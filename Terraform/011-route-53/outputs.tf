output "route53_zone_dns_servers" {
  value = ["${aws_route53_zone.zone.name_servers}"]
}

output "route53_zone_id" {
  value = "${aws_route53_zone.zone.zone_id}"
}